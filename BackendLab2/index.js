const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')

const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'home',
    password :'home',
    database : 'home'

})

connection.connect()

const express = require('express')
const { hash } = require('bcrypt')
const app = express()
const port = 4000

function authenticateToken(req, res, next) {
    const authHeader = req.headers['autorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) { return res.sendStatus(403)}
        else {
            req.user = user
            next()
        }
    })
}


/* API for Processing Buffeter Authorization*/
app.post("/login", (req, res) => {

    let username = req.query.username
    let use_password = req.query.password
    let query = `SELECT * FROM Buffeter WHERE Username= '${username}'`
    connection.query(query, (err, rows) =>{
        if (err){
            console.log(err)
            res.json({"status" : "400",
                         "message" : "Error querying from running db"  
                        })
        }else{
            let db_password = rows[0].Password
            bcrypt.compare(use_password, db_password, (err, result) => {
                if (result) {
                    let payload = {
                        "username" : rows[0].Username,
                        "user_id"  : rows[0].BuffeterID,
                        "IsAdmin"  : rows[0].IsAdmin
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d'})
                    res.send(token)
                }else {
                    res.send("Invalid username / password")
                }
            })
        }
    })
})



/* API for Registering a new Runner */
app.post("/register_user", authenticateToken, (req, res) => {
    let user_profile = req.user
    console.log(user_profile)

    let buffeter_name = req.query.buffeter_name
    let buffeter_surname = req.query.buffeter_surname
    let buffeter_username = req.query.buffeter_username
    let buffeter_pass = req.query.buffeter_pass

    bcrypt.hash(buffeter_pass, SALT_ROUNDS, (err, hash) =>{
        let query = `INSERT INTO Buffeter (BuffeterName, BuffeterSurname, Username, Password, IsAdmin )
                VALUES ('${buffeter_name}', '${buffeter_surname}', '${buffeter_username}', '${hash}', false)`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error inserting data into db"
                    })
        }
        else {
            res.json({
                        "status" : "200",
                        "message" : "Registering succesful"
            })
        }
    })
    } )

    
})

app.post("/add_rest", (req, res) => {

    let rest_name = req.query.rest_name
    let rest_locat = req.query.rest_locat
    let rest_info = req.query.rest_info

    let query = `INSERT INTO BuffetRestaurant (BuffetRestaurantName, BuffetRestaurantLocation, BuffetRestaurantInfo )
                VALUES ('${rest_name}', '${rest_locat}', '${rest_info}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error inserting data into db"
                    })
        }
        else {
            res.json({
                        "status" : "200",
                        "message" : "Adding Buffet Restaurant succesful"
            })
        }
    })
})

app.get("/list_rest",( req, res ) => {
    let query = "SELECT * FROM BuffetRestaurant"
    connection.query( query,(err,rows) => {
                if (err){
                    res.json({"status" : "400",
                                 "message" : "Error querying from running db"  
                                })
                }else{
                    res.json(rows)
                }
    })
})

app.get("/list_buffeter",( req, res ) => {
    let query = "SELECT * FROM Buffeter"
    connection.query( query,(err,rows) => {
                if (err){
                    res.json({"status" : "400",
                                 "message" : "Error querying from running db"  
                                })
                }else{
                    res.json(rows)
                }
    })
})

app.get("/list_reserv",( req, res ) => {
    let query = "SELECT * FROM BuffetReservResults"
    connection.query( query,(err,rows) => {
                if (err){
                    res.json({"status" : "400",
                                 "message" : "Error querying from running db"  
                                })
                }else{
                    res.json(rows)
                }
    })
})

app.post("/update_rest", (req, res) => {

    let rest_id    = req.query.rest_id
    let rest_name  = req.query.rest_name
    let rest_locat = req.query.rest_locat
    let rest_info  = req.query.rest_info

    let query = `UPDATE BuffetRestaurant SET
                BuffetRestaurantName = '${rest_name}',
                BuffetRestaurantLocation = '${rest_locat}',
                BuffetRestaurantInfo = '${rest_info}'
                WHERE  BuffetRestaurantID = '${rest_id}'`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error updating record"
                    })
        }
        else {
            res.json({
                        "status" : "200",
                        "message" : "Updating Buffet Restaurant succesful"
            })
        }
    })
})

app.post("/delete_rest", (req, res) => {

    let rest_id    = req.query.rest_id

    let query = `DELETE FROM BuffetRestaurant 
                 WHERE BuffetRestaurantID = '${rest_id}'`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error deleting record"
                    })
        }
        else {
            res.json({
                        "status" : "200",
                        "message" : "Deleting Buffet Restaurant succesful"
            })
        }
    })
})

app.post("/delete_reserv", (req, res) => {

    let reserv_id    = req.query.rest_id

    let query = `DELETE FROM BuffetReservResults 
                 WHERE ReservationID = '${reserv_id}'`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error deleting record"
                    })
        }
        else {
            res.json({
                        "status" : "200",
                        "message" : "Deleting Reservation succesful"
            })
        }
    })
})





app.listen(port, () => {
    console.log(`Now starting BuffetReserv System Backend  ${port}`)
})




/*
query = "SELECT  * from Buffeter";
connection.query( query, (err, rows) => {
    if (err) {
        console.log(err);
    }
    else {
        console.log(rows);
    }
});

connection.end(); */
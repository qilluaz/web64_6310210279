import BMIResult from '../components/BMIResult.js'
import {useState} from "react";

import Button from '@mui/material/Button';
import Grid  from '@mui/material/Grid';
import { Typography, Box } from '@mui/material';

function BMICalPage() {

    const [ name, setName ] = useState("");
    const [ bmiResult, setBmiResult ] = useState("");
    const [ TranslateResult, setTranslateResult ] = useState("");

    const [height, setHeight] = useState("");
    const [weight, setWeight] = useState("");

    function calculateBMI(){
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w/(h*h);
        setBmiResult(bmi);
        if(bmi>25){
            setTranslateResult("ไอต้าวก้อนนนน");
        }else if(bmi<20){
            setTranslateResult("ใครเขากินเยอะกันอะเนอะ");
        }else{
            setTranslateResult("ทรวดทรงองค์เอวแบบสับแบบปัง");
        }
    }

    return(

        <Grid container spacing={2} sx={{ marginTop : "10px "}}> 
        <Grid item xs={12}>
            <Typography variant='h4'>
                เว็บคำนวณ : BMI
            </Typography>
        </Grid>
        <Grid item xs={8}>
<Box sx={{ textAlign : "canter"}} > <br /> <br /> <br /> <br /> <br /> <br />
        ชื่อ : <input type = "text" 
        value={name} onChange={(e) => { setName(e.target.value);}}/> <br />
       
        ส่วนสูง (m) : <input type = "text" 
        value={height} onChange={(e) =>{setHeight(e.target.value);}}/>  <br /> 
       
        น้ำหนัก (kg) : <input type = "text" 
        value={weight} onChange={(e) => {setWeight(e.target.value);}}/>   <br /> <br /> <br />

        <Button variant="contained" onClick={ () => {calculateBMI()}}>
            Calculate
        </Button>
</Box>
        </Grid>
        <Grid item xs={4}>  <br /> <br /> <br /> 
        { bmiResult != 0 && 
                <div>
                    <h3>ผลการคำนวณอยู่ตรงนี้จ้าาาาา</h3>
                    <BMIResult
                        name ={ name }
                        BMI = { bmiResult }
                        result = {TranslateResult}
                    />
                </div>
            }
        </Grid>
    </Grid>

    
    );
}

export default BMICalPage;

/*<div align = "left">
        <div align = "center">
            <br/>
            <h1>ยินดีต้อนรับสู่เว็บคำนวณ BMI</h1>
            <br/>

            <h2>ชื่อ : <input type = "text" value={name} onChange={(e) => { setName(e.target.value);}}/>
            </h2>

            <br/>
            <h3>ส่วนสูง (m) : <input type = "text" value={height} onChange={(e) =>{setHeight(e.target.value);}}/>
            </h3>

            <h3>น้ำหนัก (kg) : <input type = "text" value={weight} onChange={(e) => {setWeight(e.target.value);}}/></h3>
            <br />
            <button onClick={(e) => {calculateBMI()}}>Calculate</button>
            <br/>
            <br/>
            <br/>

            { bmiResult != 0 && 
                <div>
                    <h3>ผลการคำนวณอยู่ตรงนี้จ้าาาาา</h3>
                    <BMIResult
                        name ={ name }
                        BMI = { bmiResult }
                        result = {TranslateResult}
                    />
                </div>
            }
                    
        </div>
    </div>*/
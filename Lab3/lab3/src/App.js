import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import Pudding from './components/Pudding';


function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      <Pudding />
      <Footer />
    </div>
  );
}

export default App;

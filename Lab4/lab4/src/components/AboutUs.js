import './AboutUs.css'
//const ABoutUs = () = =>{}
function AboutUs (props){


    return (
        <div class="BG">
            <h2> พัฒนาโดย { props.name } </h2>
            <h3> ผู้มาจาก { props.province }.</h3>
            <br />
        </div>

    );
}

export default AboutUs;
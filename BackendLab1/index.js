const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/bmi', (req, res) => {

    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result = {}

    if (!isNaN(weight) && !isNaN(height) ){
        let bmi = weight / (height * weight)
        result = {
            "status" : 200,
            "bmi" : bmi
        }
    }
    else {

        result = {
            "status" : 400,
            "message" : "Weight or Height is not a number"
        }
    }
    
    res.send(JSON.stringify(result))

})


app.get('/triangle', (req, res) => {

    let base = parseFloat(req.query.base)
    let high = parseFloat(req.query.high)
    var result = {}

    if (!isNaN(base) && !isNaN(high) ){
        let area =  (base*high) / 2
        result = {
            "status" : 200,
            "area" : area
        }
    }else {

        result = {
            "status" : 400,
            "message" : "Base or high is not a number"
        }
    }

    res.send(JSON.stringify(result))
        
})


app.post('/score', (req, res) => {

    let name = req.query.name
    let score = parseFloat(req.query.score)
    var result = {}

    if (!isNaN(score) && score <= 100){
        let grade = "Default"
        if(score >= 85){
            grade = "A"
        }
        else if(score >= 75){
            grade = "B"
        }
        else if(score >= 65){
            grade = "C"
        }
        else if(score >= 45){
            grade = "D"
        }
        else
            grade = "E"

        result = {
            "status" : 200,
            "Grade"  : grade
        }
               
           
       }
       else{
           result = {
               "status" : 400,
               "message" : "Score is over than 100 or is not number"
           }
       }

    

    res.send(JSON.stringify(result))

})




app.get('/hello', (req, res) => {
    res.send('D kub '+ req.query.name)
  })
  

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})